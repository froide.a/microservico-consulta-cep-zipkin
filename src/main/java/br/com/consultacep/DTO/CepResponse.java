package br.com.consultacep.DTO;

import br.com.consultacep.gateway.Cep;

public class CepResponse {

    private String cep;
    private String logradouro;
    private String bairro;
    private String localidade;
    private String uf;

    public CepResponse() {
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public CepResponse converterParaCepResponse(Cep cep) {
        CepResponse cepResponse = new CepResponse();
        cepResponse.setCep(cep.getCep());
        cepResponse.setLogradouro(cep.getLogradouro());
        cepResponse.setBairro(cep.getBairro());
        cepResponse.setLocalidade(cep.getLocalidade());
        cepResponse.setUf(cep.getUf());
        return cepResponse;
    }
}
