package br.com.consultacep.service;

import br.com.consultacep.gateway.ViaCep;
import br.com.consultacep.gateway.Cep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

@Service
public class CepService {

    @Autowired
    private ViaCep viaCep;

    @NewSpan(name = "consultarCep")
    public Cep consultarCep(String cep) {
        return viaCep.obterCep(cep);
    }
}
