package br.com.consultacep.gateway;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CEP", url = "https://viacep.com.br/")
public interface ViaCep {

    @GetMapping("/ws/{cep}/json/")
    Cep obterCep(@PathVariable String cep);
}
