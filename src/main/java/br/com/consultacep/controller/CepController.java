package br.com.consultacep.controller;

import br.com.consultacep.DTO.CepResponse;
import br.com.consultacep.service.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cep")
public class CepController {

    @Autowired
    private CepService cepService;


    @GetMapping("/{cep}")
    public CepResponse consultarCep(@PathVariable String cep) {
        CepResponse cepResponse = new CepResponse();
        return cepResponse.converterParaCepResponse(cepService.consultarCep(cep));
    }

}
